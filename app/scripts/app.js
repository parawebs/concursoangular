'use strict';

/**
 * @ngdoc overview
 * @name lottoApp
 * @description
 * # lottoApp
 *
 * Main module of the application.
 */
angular.module('lottoApp', [
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch',
    'firebase',
    'firebase.utils',
    'facebook'
  ]);
