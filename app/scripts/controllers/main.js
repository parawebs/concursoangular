'use strict';

/**
 * @ngdoc function
 * @name lottoApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the lottoApp
 */
angular.module('lottoApp')
  .controller('MainCtrl', function ($scope) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
