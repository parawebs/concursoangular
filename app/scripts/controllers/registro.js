'use strict';

/**
 * @ngdoc function
 * @name lottoApp.controller:RegistroCtrl
 * @description
 * # RegistroCtrl
 * Controller of the lottoApp
 */
angular.module('lottoApp').config(function(FacebookProvider) {
  // Set your appId through the setAppId method or
  // use the shortcut in the initialize method directly.
  FacebookProvider.init('348846518625873');
}).controller('RegistroCtrl', function ($scope, fbutil, $timeout,Facebook) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
    // Define user empty data :/
    $scope.user = {};

    // Defining user logged status
    $scope.logged = false;
    $scope.exist = false;
    $scope.exito = true;
    // And some fancy flags to display messages upon user status change
    $scope.byebye = false;
    $scope.salutation = false;
    
    //$scope.registrar = false;
    /**
     * Watch for Facebook to be ready.
     * There's also the event that could be used
     */
    $scope.$watch(
      function() {
        return Facebook.isReady();
      },
      function(newVal) {
        if (newVal)
          $scope.facebookReady = true;
      }
    );

    var userIsConnected = false;

    Facebook.getLoginStatus(function(response) {
      if (response.status == 'connected') {
        userIsConnected = true;
      }
    });

    /**
     * IntentLogin
     */
    $scope.IntentLogin = function() {
      if(!userIsConnected) {
        $scope.login();
      }
    };

    /**
     * Login
     */
    $scope.login = function () {
      Facebook.login(function (response) {
          if (response.status == 'connected') {
            $scope.loginTrue = true;
            $scope.me();
          }

        }, {scope: 'email'}
      );
    };

    /**
     * me
     */
    $scope.me = function() {
      console.log("calling me")
      Facebook.api('/me', function(response) {
        /**
         * Using $scope.$apply since this happens outside angular framework.
         */
         $scope.$apply(function() {
          $scope.user = response;
          $scope.email =  response.email;
          
        });

      });
    };

    /**
     * Logout
     */
    $scope.logout = function() {
      Facebook.logout(function() {
        $scope.$apply(function() {
          $scope.user   = {};
          $scope.logged = false;
        });
      });
    }

    /**
     * Taking approach of Events :D
     */
    $scope.$on('Facebook:statusChange', function(ev, data) {
      console.log('Status: ', data);
      if (data.status == 'connected') {
        $scope.$apply(function() {
          $scope.logged = true;
          $scope.me();
          $scope.salutation = true;
          $scope.registrar = false;
          $scope.byebye     = false;
        });
      } else {
        $scope.$apply(function() {
          $scope.salutation = false;
          $scope.byebye     = true;

          // Dismiss byebye message after two seconds
          $timeout(function() {
            $scope.byebye = false;
          }, 2000)
        });
      }


    });

    $scope.registros = fbutil.syncArray('registros', {limit: 10});

    // display any errors
    $scope.registros.$loaded().catch(alert);

    // provide a method for adding a message
    $scope.addMessage = function(email,telefono,name,fbid) {
      var udata = {email: email,phone:telefono,name:name,f_id:fbid};
      function tryCreateUser(userId, userData) {
        var usersRef = new Firebase("https://concursofelipe.firebaseio.com/registros");
        usersRef.child(userId).transaction(function(currentUserData) {
          if (currentUserData === null)
            return userData;
        }, function(error, committed) {
          $scope.exist = !committed;
          $scope.exito = !committed;
          console.log(committed);
        });

      }

      tryCreateUser(fbid, udata);
      
    };

  });




